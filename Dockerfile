FROM openjdk:8-jre-alpine

WORKDIR /data

RUN addgroup -g 1000 minecraft \
  && adduser -Ss /bin/false -u 1000 -G minecraft minecraft \
  && chown minecraft:minecraft /srv /data 

USER minecraft

COPY --chown=minecraft server.properties ./
COPY --chown=minecraft server.properties /srv/server.properties.template

ADD --chown=minecraft \
    https://launcher.mojang.com/v1/objects/35139deedbd5182953cf1caa23835da59ca3d7cd/server.jar \
    /srv/

RUN wget -O - https://github.com/itzg/mc-server-runner/releases/download/1.5.0/mc-server-runner_1.5.0_linux_amd64.tar.gz | tar zxf - -C /srv mc-server-runner

EXPOSE 25565 25575

COPY --chown=minecraft entrypoint.sh /srv/
RUN chmod +x /srv/entrypoint.sh

ENTRYPOINT [ "/srv/entrypoint.sh" ]